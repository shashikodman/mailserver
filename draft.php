<?php
include_once('connection.php');

$id=$_SESSION['sid'];
$_SESSION['currentpage'] = 'draft';

$sql="SELECT * FROM draft where user_id='$id'";
$dd=mysqli_query($con,$sql);

echo "
	<div class=\"row pageheader\">
		<h4>Draft</h4>
	</div>

	<form>
		<div class=\"row mailsheader grey lighten-2\">
			<button type=\"submit\" name=\"draftdelete\" id=\"draftdelete\">
				<span class=\"material-icons grey-text text-darken-2\" style=\"vertical-align: middle;\">delete</span>
				<span class=\"grey-text text-darken-2\" style=\"vertical-align: middle;\">Delete selected</span>
			</button>
		</div>";
while(list($draftId,$u_id,$sub,$file,$msg,$date)=mysqli_fetch_array($dd))
{	
	echo "
		<div class=\"row mails\">	
			<div class=\"col s1 m1 l1\">
				<input type='checkbox' name='ch[]' id='$draftId' value='$draftId'/>
				<label for=\"$draftId\"></label>
			</div>
			<a href=\"HomePage.php?condraft=$draftId\">
			<div class=\"col s3 m3 l2 black-text\">
					<span class=\"truncate\">$sub</span>
				</div>
				<div class=\"col s2 m4 l6 grey-text text-darken-2\">
					<span class=\"truncate\">$msg</span>
				</div>
				<div class='col s1 m1 l1'>";

				if(!empty($file)) {
					echo "
					
						<span class='right-align material-icons grey-text text-darken-2' style='font-size: 1.2rem; vertical-align: middle;'>perm_media</span>
					";
					}

				echo "
				</div>	
				<div class=\"col s5 m3 l2 right-align\" style=\"padding-right: 20px;\">
					$date
				</div>
			</a>
	  	</div>"; 
 
}
	
echo "</form>";

?>
