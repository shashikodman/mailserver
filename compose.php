<?php
@$to=$_POST['to'];
@$sub=$_POST['sub'];
@$msg=$_POST['msg'];

$id=$_SESSION['sid'];

@$file_name=$_FILES['file']['name'];
@$file_size=$_FILES['file']['size'];
@$file_tmp=$_FILES['file']['tmp_name'];
$targetirectory = "userFiles/$id/";

if(!file_exists($targetirectory)) {
	mkdir($targetirectory);
}

$statusflag=3;
if(@$_POST['send'])
{
	echo "SEnd";
	if($to=="" || $sub=="" || $msg=="")
	{ 
		$err= "fill the related data first";
	}
	else
	{	
		$d=mysqli_query($con, "SELECT * FROM userinfo where user_name='$to'")
			or die("Error querying: ".mysqli_error($con));
		$row=mysqli_num_rows($d);
		if($row==1) {	
			if($file_size<=26214400) {
				move_uploaded_file($file_tmp, $targetirectory.$file_name);
				mysqli_query($con, "INSERT INTO usermail(rec_id,sen_id,sub,msg,attachement,recDT) values('$to','$id','$sub','$msg','".$file_name."',sysdate())")
					or die('Error querying: '.mysqli_error($con));
				$statusflag = 1;	
				$err= "Message successfully sent!";
			}
			else {
				$statusflag = 0;
				$err="File size exceeded 25MB limit!";
			}
		}	
		else
		{
			$sub=$sub."--"."msg failed";
			mysqli_query($con, "INSERT INTO usermail(rec_id,sen_id,sub,msg,attachement,recDT) values('$to','$id','$sub','$msg','$file_name',sysdate())");
			$statusflag = 0;
			$err= "Message failed!";
		}	
	}
}	

if(@$_REQUEST['save'])
{
	if($sub=="" || $msg=="")
	{
	$err= "<font color='red'>fill subject and msg first</font>";
	}
	
	else
	{	
		if($file_size<=26214400) {
			move_uploaded_file($file_tmp, $targetirectory.$file_name);	
			$query="INSERT INTO draft(user_id,sub,attach,msg,date) values('$id','$sub','$file_name','$msg',sysdate())";
			mysqli_query($con,$query)
				or die("Error querying: ".mysqli_error($con));
			$statusflag = 1;
			$err= "Message saved!";
		}
		else {
			$statusflag = 0;
			$err="File size exceeded 25MB limit!";
		}	
	}
}	

	
?>

<?php 
	if($statusflag==1) 
		echo "
			<div class=\"z-depth-2\" style=\"margin-top: 5px;\">
				<div class=\"row green status\" style=\" margin-bottom: 0px;\">
					$err
				</div>
			</div>";
	else if($statusflag==0)
		echo "
			<div class=\"z-depth-2\" style=\"margin-top: 5px;\">
				<div class=\"row red status\" style=\" margin-bottom: 0px;\">
					$err
				</div>
			</div>";
	else echo "";		
?>			
				
<form method="post" enctype="multipart/form-data">
	<div class="row pageheader">
		<div class="col s12 m8">
			<h4>Compose Mail</h4>
		</div>	
		<div class="col s12 m2" style="margin-top: 14px;">
			<input type="submit" name="save" class="col s12 btn waves-effect grey lighten-2 black-text right" value="Draft">
		</div>	
		<div class="col s12 m2" style="margin-top: 14px;">
			<input type="submit" name="send" class="col s12 btn waves-effect green right" value="Send" />
		</div>	
	</div>
	
	<div style="padding: 30px 40px 0px 40px;">
		
		<div class="row input-field">
			<input type="text" name="to" id="to" required>
			<label for="to">To</label>
		</div>
		<div class="row input-field">
			<input type="text" name="cc" id="cc">
			<label for="cc">Cc</label>
		</div>
		<div class="row input-field">
			<input type="text" name="sub" id="sub" required>
			<label for="sub">Subject</label>
		</div>
		<div class="row file-field input-field">
			<div class="btn blue-grey darken-3 waves-effect">
				<span>File</span>
				<input type="file" name="file" id="file">
			</div>	
		</div>
		<div class="row input-field">
			<textarea type="text"  class="materialize-textarea" name="msg" id="msg" required></textarea> 
			<label for="msg">Message</label>
		</div>
		
	</div>
</form>
