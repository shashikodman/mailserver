<?php
error_reporting(E_ALL);
	session_start();
	if($_SESSION['sid']=="")
	{
	header('Location: index.php');
	}
	$id=$_SESSION['sid'];
	$img = $_SESSION['imagePath'];
	include_once('connection.php');

	//for logout
	@$chk=$_GET['chk'];
	if($chk=="logout")
	{
	unset($_SESSION['sid']);
	header('Location:index.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
	<header>
	<div class="navbar-fixed">
		<nav class="white">
			<div class="nav-wrapper">
				<a href="HomePage.php" class="brand-logo grey-text text-darken-2"><span class="blue-text text-lighten-2">Mail</span>Server</a>

				<a href="#" data-activates="slide-out" class="button-collapse grey-text text-darken-2"><span class="material-icons" style="font-size: 2rem; vertical-align: middle;">menu</span><span class="material-icons" style="font-size: 1rem; vertical-align: middle;">play_arrow</span></a>

				<ul class="right">
					<li class="grey lighten-2"><div style="width: 1px; height: 56px;"></div></li>
					<li>
						<a href="#" class="dropdown-button" data-activates="dropdown1">
							<img src="<?=$img;?>" class="circle" style="vertical-align: middle; height: 30px;">
							<span class="black-text" style="padding-left: 8px;"><?php echo $_SESSION['name']; ?></span>
						</a>
						<ul id="dropdown1" class="dropdown-content">
							<li><a href="HomePage.php?chk=profile">Profile</a></li>
							<li class="divider"></li>
							<li><a href="HomePage.php?chk=logout">Log Out</a></li>
					  	</ul>
					</li>
				</ul>

				<ul id="slide-out" class="side-nav fixed">
					<div class="sideheader">
					  	<div class="row" style="margin-bottom: 0;">
							<div class="col s4" style="padding: 30px 0px 0px 20px;">
								<img class="circle responsive-img" style="width: 50px;" src="<?=$img;?>">
							</div>
							<div class="col s8" style="padding: 18px 0 0 0;">
								<div class="row">	
									<span class="truncate" style="font-size: 14px; font-weight: 200;">Welcome, <?php echo $_SESSION['name']; ?></span>
								</div>
							</div>
					  	</div>
					</div>
					<li>
						<a href="HomePage.php?chk=compose" class="btn white black-text waves-effect"><span>Compose</span><i class="material-icons">mode_edit</i></a>
					</li>
					<li <?php if($chk=='inbox') echo "class=\"active\"";?> >
						<a href="HomePage.php?chk=inbox" class="white-text" style="font-weight: 100;"><i class="material-icons grey-text">email</i>Inbox</a>
					</li>
					<li <?php if($chk=='sent') echo "class=\"active\"";?> >
						<a href="HomePage.php?chk=sent" class="white-text" style="font-weight: 100;"><i class="material-icons grey-text">message</i>Sent</a>
					</li>
					<li <?php if($chk=='draft') echo "class=\"active\"";?>>
						<a href="HomePage.php?chk=draft" class="white-text" style="font-weight: 100;"><i class="material-icons grey-text">description</i>Draft</a>
					</li>
					<li <?php if($chk=='trash') echo "class=\"active\"";?>>
						<a href="HomePage.php?chk=trash" class="white-text" style="font-weight: 100;"><i class="material-icons grey-text">delete</i>Trash</a>
					</li>
					<li <?php if($chk=='update') echo "class=\"active\"";?>>
						<a href="HomePage.php?chk=updnews" class="white-text" style="font-weight: 100;"><i class="material-icons grey-text">update</i>News Update</a>
					</li>
				</ul>

			</div>
		</nav>
	</div>	

				

	</header>

	<div class="wrapper">
			
		<?php
		@$id=$_SESSION['sid'];
		@$chk=$_REQUEST['chk'];


			if($chk=="vprofile")
			{
			include_once("editProfile.php");
			}
			if($chk=="compose")
			{
			include_once('compose.php');
			}
			if($chk=="sent")
			{
			include_once('sent.php');
			}
			if($chk=="inbox")
			{
			include_once('inbox.php');
			}
			if($chk=="setting")
			{
			include_once('setting.php');
			}
			if($chk=="chngPass")
			{
			include_once('chngPass.php');
			}
			if($chk=="chngthm")
			{
			include_once('chngthm.php');
			}
			if($chk=="draft")
			{
			include_once('draft.php');
			}
			if($chk=="updnews")
			{
			include_once('latestupd.php');
			}
			if($chk=="profile")
			{
				include_once('profile.php');
			}
			
		?>
		
		<!--inbox -->
		<?php
		$id=$_SESSION['sid'];
		@$coninb=$_GET['coninb'];
			
		if($coninb)
		{	$_SESSION['homeflag'] = 0;
			$read = mysqli_query($con, "UPDATE usermail SET msgread=1 WHERE rec_id='$id' AND mail_id='$coninb' ")
				or die(mysqli_error($con));
			$sql="SELECT * FROM usermail where rec_id='$id' and mail_id='$coninb'";
			$dd=mysqli_query($con, $sql);
			$row=mysqli_fetch_object($dd);
			echo "
				<div class=\"row\" style=\"padding: 20px;\">
					<h4>$row->sub</h4>
					<hr><br>
					<div class=\"row\">
						<div class=\"col s3\" style=\"width: 55px;\">
							<img src=\"images/c.png\" class=\"circle\" style=\"width: 45px;\">
						</div>
						<div class=\"col s9\">
							<span style=\"font-size: 15px;\"><b>$row->sen_id</b><span><br>
							<span class=\"grey-text\" style=\"font-size: 12px;\">To me<span>
						</div>
					</div>
					<div class=\"row\" style=\"padding-left: 15px;\">
						<p style=\"font-size: 16px;\">
							$row->msg
						</p>
					</div>";

					if(!empty($row->attachement)) {
						echo "
						<div class='row' style='padding-left: 15px;'>
							<div id='file'>
								<div id='fileimage'>
									<img src='images/file.png' style='width: 60px;' />
								</div>
								<div class='grey lighten-2 center-align'>
									<span class='truncate' style='padding: 8px 0;'>
										<a href='download.php?filename=".$row->attachement."&sen_id=".$row->sen_id."'><span class='material-icons'>play_for_work</span></a>	$row->attachement
									</span>
								</div>
							</div>
						</div>";
					}

				echo "	
				</div>";
				
		}
			
			
		@$cheklist=$_REQUEST['ch'];
		if(isset($_GET['delete']))
		{	
			if(isset($cheklist)) {
				foreach($cheklist as $v)
				{
				
				$d="DELETE from usermail where mail_id='$v'";
				mysqli_query($con, $d);
				}
				echo "
					<script>
						alert('Messages deleted successfully');
						window.location.href = \"HomePage.php?chk=".$_SESSION['currentpage']."\";
					</script>";
			}
			else
				echo "<script>window.location.href=\"HomePage.php?chk=".$_SESSION['currentpage']."\";</script>";
		}	
		
		?>

		<?php
		// For deleting draft messages

		@$cheklist=$_REQUEST['ch'];
		if(isset($_GET['draftdelete']))
		{	
			if(isset($cheklist)) {
				foreach($cheklist as $v)
				{
				
				$d="DELETE from draft where draft_id='$v'";
				mysqli_query($con, $d);
				}
				echo "
					<script>
						alert('Messages deleted successfully');
						window.location.href = \"HomePage.php?chk=".$_SESSION['currentpage']."\";
					</script>";
			}
			else
				echo "<script>window.location.href=\"HomePage.php?chk=".$_SESSION['currentpage']."\";</script>";
		}	
		
		?>
		
	<!--sent box-->
	<?php
		$id=$_SESSION['sid'];
		@$consent=$_GET['consent'];
			
		if($consent)
		{	$_SESSION['homeflag'] = 0;
			$sql="SELECT * FROM usermail where sen_id='$id' and mail_id='$consent'";
			$dd=mysqli_query($con, $sql);
			$row=mysqli_fetch_object($dd);
			echo "
				<div class=\"row\" style=\"padding: 20px;\">
					<h4>$row->sub</h4>
					<hr><br>
					<div class=\"row\">
						<div class=\"col s3\" style=\"width: 55px;\">
							<img src=\"images/c.png\" class=\"circle\" style=\"width: 45px;\">
						</div>
						<div class=\"col s9\">
							<span style=\"font-size: 15px;\"><b>$row->rec_id</b><span><br>
							<span class=\"grey-text\" style=\"font-size: 12px;\">To me<span>
						</div>
					</div>
					<div class=\"row\" style=\"padding-left: 15px;\">
						<p style=\"font-size: 16px;\">
							$row->msg
						</p>
					</div>";

					if(!empty($row->attachement)) {
						echo "
						<div class='row' style='padding-left: 15px;'>
							<div id='file'>
								<div id='fileimage'>
									<img src='images/file.png' style='width: 60px;' />
								</div>
								<div class='grey lighten-2 center-align'>
									<span class='truncate' style='padding: 8px 0;'>
										<a href='download.php?filename=".$row->attachement."&sen_id=".$row->sen_id."'><span class='material-icons'>play_for_work</span></a>	$row->attachement
									</span>
								</div>
							</div>
						</div>";
					}

				echo "	
				</div>";
		}	
			
		/*	
		@$cheklist=$_REQUEST['ch'];
		if(isset($_GET['delete']))
		{
			foreach($cheklist as $v)
			{
			$d="DELETE from usermail where mail_id='$v'";
			mysql_query($d);
			}
			echo "msg deleted";
		}
		*/
		
	?>	

<!--draft box-->
	<?php
		$id=$_SESSION['sid'];
		@$condraft=$_GET['condraft'];
			
		if($condraft)
		{	$_SESSION['homeflag'] = 0;
			$sql="SELECT * FROM draft where user_id='$id' and draft_id='$condraft'";
			$dd=mysqli_query($con, $sql);
			$row=mysqli_fetch_object($dd);
			echo "
				<div class=\"row\" style=\"padding: 20px;\">
					<h4>$row->sub</h4>
					<hr><br>
					<div class=\"row\">
						<div class=\"col s3\" style=\"width: 55px;\">
							<img src=\"images/c.png\" class=\"circle\" style=\"width: 45px;\">
						</div>
						<div class=\"col s9\">
							<span style=\"font-size: 15px;\"><b>$row->rec_id</b><span><br>
							<span class=\"grey-text\" style=\"font-size: 12px;\">To <span>
						</div>
					</div>
					<div class=\"row\" style=\"padding-left: 15px;\">
						<p style=\"font-size: 16px;\">
							$row->msg
						</p>
					</div>";

					if(!empty($row->attach)) {
						echo "
						<div class='row' style='padding-left: 15px;'>
							<div id='file'>
								<div id='fileimage'>
									<img src='images/file.png' style='width: 60px;' />
								</div>
								<div class='grey lighten-2 center-align'>
									<span class='truncate' style='padding: 8px 0;'>
										<a href='download.php?filename=".$row->attach."&sen_id=".$row->user_id."'><span class='material-icons'>play_for_work</span></a>	$row->attach
									</span>
								</div>
							</div>
						</div>";
					}

				echo "	
				</div>";
		}	
			
		/*	
		@$cheklist=$_REQUEST['ch'];
		if(isset($_GET['delete']))
		{
			foreach($cheklist as $v)
			{
			$d="DELETE from usermail where mail_id='$v'";
			mysql_query($d);
			}
			echo "msg deleted";
		}
		*/
		
	?>	

	<?php  

	if($chk=='') {
			if($_SESSION['homeflag']==1) {
				echo " 
				<div style=\"height: 600px; background-color: #2E3641; margin-top: 13px;\">
					<div class=\"container\" style=\"padding-top: 40px;\">
						<div class=\"row white-text\" style=\"background: rgba(255,255,255,0.2); padding: 40px 0 42px 0; margin-top: 5px; height: 500px;\">
							<h3 align=\"center\">Welcome <span class=\"blue-text text-lighten-2\">".$_SESSION['name']."</span>!</h3>
							<p class=\"center-align grey-text text-lighten-2\" style=\"font-size: 18px;\">Enjoy using MailServer!</p>
							<div class=\"center-align\" style=\"margin-top: 50px;\">
								<img src=\"images/y.png\" style=\"width: 120px;\">
							</div>
							<div class=\"row center-align\" style=\"margin-top: 120px;\">
								<a href=\"HomePage.php?chk=inbox\" class=\"btn blue darken-2\">View Inbox</a>
							</div>
						</div>		
					</div>
				</div>";	
				
			}
		}
		$_SESSION['homeflag'] = 1;

	?>
	

	</div><!--End wrapper-->
	
	<footer class="page-footer blue-grey">
		<div class="footer-copyright blue-grey darken-4">
			<div class="container">
				Copyright © 2017 MailServer All rights reserved.
			</div>
		</div>
	</footer>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/materialize.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
		
