<?php
session_start();
include_once('connection.php');
if(isset($_POST['signIn']))
{
	if($_POST['id']=="" || $_POST['pwd']=="")
	{
	$err="Type your email and password!";
	}
	else
	{
		$d=mysqli_query($con, "SELECT * FROM userinfo where user_name='".$_POST['id']."'")or die(mysqli_error($con));
		if(mysqli_num_rows($d)>0){
		  $row=mysqli_fetch_object($d);
		  $fid=$row->user_name;
		  $fpass=$row->password;
		  if($fid==$_POST['id'] && $fpass==$_POST['pwd'])
		  {
		    $_SESSION['sid']=$_POST['id'];
		    $_SESSION['name'] = $row->name;
		    $_SESSION['imagePath'] = "userImages/".$_POST['id']."/$row->image";
		    $_SESSION['homeflag'] = 1;
		    header('location:HomePage.php');
		  }
		  else
		  {
		    $err="Invalid email or password!";
		  }
		}
		else
		{
		  $err="Invalid email or password!";
		}
	}
}
?>


<div class="row" id="loginheader">
  <div class="row">
    <h4 style="">Mail Server</h4>
  </div>
  <div class="row">
    <p style="text-align:center; margin-top:-20px; padding-bottom: 10px; color:grey;">
    	Dear user, please login below to use MailServer.
    </p>
  </div>
</div>

<div class="row center-align" style="margin-bottom: 0; z-index: 100;">
  <img  src="images/z.jpg">
</div>

<div class="row" id="loginbody">
  <form method="post">
    <div class="col s10 m6 l4 offset-s1 offset-m3 offset-l4">
      <div class="row center-align">
        <?php 
          if(!empty($err)) {
            echo "<span class='red-text'>$err</span>";
          }
        ?>
      </div>
      <div class="row" style="margin-bottom: 8px;">
        <input type="text" name="id" placeholder="Username" class="loginform" required>
      </div>
      <div class="row">
        <input type="password" name="pwd" placeholder="Password" class="loginform" required>
      </div>
      <div class="row" style="margin-left: 4%;">
        <button type="submit" name="signIn" class="btn blue col s12">Login</button>
      </div> 
      <div class="row">
        <p class="center-align grey-text">New user? Click <a href="index.php?chk=4">here</a> to sign up. &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<a href="index.php?chk=99">Forgot password</a></p>
      </div>
    </div>  
  </form>
</div>

