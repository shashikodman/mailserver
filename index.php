<?php 
	//error_reporting(1);	
	include_once('connection.php');
	@$chk=$_REQUEST['chk'];
?>
<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
	<link rel="stylesheet" type="text/css" href="css/style1.css">

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<script type="text/javascript" src="js/jquery.min.js"></script>
	

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="js/materialize.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<header>
	<div class="navbar-fixed">
		<nav class="white">
			<div class="nav-wrapper">
				<a href="index.php" class="brand-logo grey-text text-darken-2"><span class="blue-text text-lighten-2">Mail</span>Server</a>

				<a href="#" data-activates="slide-out" class="button-collapse grey-text text-darken-2"><span class="material-icons" style="font-size: 2rem; vertical-align: middle;">menu</span><span class="material-icons" style="font-size: 1rem; vertical-align: middle;">play_arrow</span></a>

				<ul class="right hide-on-med-and-down">
					<li <?php if($chk=='') echo "class=\"active\"";?> >
						<a href="index.php" class="black-text">Home</a>
					</li>
					<li <?php if($chk==2) echo "class=\"active\"";?> >
						<a href="index.php?chk=2" class="black-text">About Us</a>
					</li>
					<li <?php if($chk==3) echo "class=\"active\"";?> >
						<a href="index.php?chk=3" class="black-text">Login</a>
					</li>
					<li <?php if($chk==4) echo "class=\"active\"";?> >
						<a href="index.php?chk=4" class="black-text">New User?</a>
					</li>
					<li <?php if($chk==6) echo "class=\"active\"";?> >
						<a href="index.php?chk=6" class="black-text">Contact Us</a>
					</li>
				</ul>

				<ul id="slide-out" class="side-nav">
					<li <?php if($chk=='') echo "class=\"active\"";?> >
						<a href="index.php" class="white-text">Home</a>
					</li>
					<li <?php if($chk==2) echo "class=\"active\"";?> >
						<a href="index.php?chk=2" class="white-text">About Us</a>
					</li>
					<li <?php if($chk==3) echo "class=\"active\"";?> >
						<a href="index.php?chk=3" class="white-text">Login</a>
					</li>
					<li <?php if($chk==4) echo "class=\"active\"";?> >
						<a href="index.php?chk=4" class="white-text">New User?</a>
					</li>
					<li <?php if($chk==6) echo "class=\"active\"";?> >
						<a href="index.php?chk=6" class="white-text">Contact Us</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	</header>

	<div class="wrapper">
	
	<?php
	
	if($chk=="")
	{
	?>	

	<div style="height: 100%; background-color: #2E3641; margin-top: 20px;">
		<div class="container" style="padding-top: 40px;">
			<div class="row white-text" style="background: rgba(255,255,255,0.2); padding: 40px 0 42px 0; margin-top: 5px; height: 500px;">
				<h3 align="center">Welcome to <span class="blue-text text-lighten-2">Mail</span><span class="grey-text text-darken-3">Server</span></h3>
				<p class="center-align grey-text text-lighten-2" style="font-size: 18px;">Enjoy using MailServer!</p>
				<div class="center-align" style="margin-top: 50px;">
					<img src="images/y.png" style="width: 120px;">
				</div>
				<div class="row center-align" style="margin-top: 120px;">
					<a href="index.php?chk=3" class="btn blue darken-2">Login Now?</a>
				</div>
			</div>
			<style>
				.collapsible-body{
						background-color:white;
					}
			</style>
			<div>
					
			    <ul class="collapsible" data-collapsible="accordion">
						<li style="background-color:white;"><h4>News</h4></li>
						<?php
							$query2 = "SELECT * FROM  `latestupd` order by upd_id desc";

              $result2 = mysqli_query($con,$query2)
                or die("Error querying database: " . mysqli_error());
              while($row2 = mysqli_fetch_array($result2)){
						?>
						<li>
							<div class="collapsible-header"><i class="material-icons"></i><?=$row2[1];?></div>
							<div class="collapsible-body"><span><?=$row2[2];?></span></div>
						</li>
						<?php } ?>
					</ul>
  
			</div>		
		</div>
	</div>	
	<?php
	}
	if($chk==4)
	{
	include_once('regis.php');
	}
	if($chk==3)
	{
	include_once('login.php');
	}
	if($chk==5)
	{
	include_once('welcome.php');
	}
	if($chk==2)
	{
	include_once('aboutus.php');
	}
	if($chk=="6")
	{
	include_once('contactus.php');
	}
	if($chk=="7")
	{
	include_once('latestupdDisp.php');
	}
	if($chk==99)
	{
	include_once('forgot.php');
	}
	
	?>
    
    </div><!--End wrapper-->
	
	<footer class="page-footer blue-grey">
		<div class="footer-copyright blue-grey darken-4">
			<div class="container">
				Copyright © 2017 MailServer All rights reserved.
			</div>
		</div>
	</footer>
</body>
</html>
		
